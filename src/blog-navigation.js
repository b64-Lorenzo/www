import { getPermalink, getBlogPermalink } from './utils/permalinks';

export const headerData = {
  name:"ByteMath",
  home: getBlogPermalink(),
  position:"center",
  links:[
        { text: 'Tutorials', href: getPermalink('tutorials', 'category') },
        { text: 'Documentation', href: getPermalink('documentation', 'category') },
        {
          text: 'About',
          href: getPermalink("bytemath/about-us"),
        },
        {
          text: 'Contact',
          href: getPermalink("bytemath/contact-us"),
        },
    ]
};

export const footerData = {
  name: "ByteMath",
  links: [
    {
      title: 'Product',
      links: [
        { text: 'Tutorials', href: getPermalink('tutorials', 'category') },
        { text: 'Documentation', href: getPermalink('documentation', 'category') },
        {
          text: 'About',
          href: getPermalink("bytemath/about-us"),
        },
        {
          text: 'Contact',
          href: getPermalink("bytemath/contact-us"),
        },
      ],
    },
    {
      title: 'Other',
      links: [
        {
          text: 'FiMa',
          href: getPermalink('/homes/fima'),
        },
        {
          text: 'Personal',
          href: getPermalink('/homes/personal'),
        },
      ],
    },
    {
      title: 'Follow Us!',
      links: [
        { ariaLabel: 'Instagram', icon: 'tabler:brand-instagram', href: 'https://instagram.com/bytemath_' },
        { ariaLabel: 'Youtube', icon: 'tabler:brand-youtube', href: 'https://www.youtube.com/channel/UCnyveEbLc5HYP2JNhjz6WtQ' },
        { ariaLabel: 'Gitlab', icon: 'tabler:brand-gitlab', href: 'https://gitlab.com/b64-Lorenzo/www' },
      ],
      isFlex: true,
    },
    {
      title: 'Legal',
      links: [
        { text: 'Terms', href: getPermalink('/terms') },
        { text: 'Privacy Policy', href: getPermalink('/privacy') },
      ],
    },
  ],
  secondaryLinks: [
    
  ],
};
