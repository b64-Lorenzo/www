import { getPermalink, getBlogPermalink } from './utils/permalinks';

export const headerData = {
  links: [
    // {
    //   text: 'ByteMath',
    //   href: 'https://bytemath.lorenzozambelli.it',
    //   links: [
    //     {
    //       text: 'HomePage',
    //       href: 'https://bytemath.lorenzozambelli.it',
    //     },
    //     {
    //       text: 'Blog List',
    //       href: 'https://bytemath.lorenzozambelli.it/blog',
    //     },
    //     {
    //       text: 'Projects List',
    //       href: 'https://bytemath.lorenzozambelli.it/projects',
    //     },
    //     {
    //       text: 'Tag Page',
    //       href: 'https://bytemath.lorenzozambelli.it/tags',
    //     },
    //   ],
    // },
    {
      text: 'FiMa',
      href: getPermalink('/homes/fima'),
    },
    {
      text: 'Personal',
      href: getPermalink('/homes/personal'),
    },
    {
      text: 'Pages',
      links: [
        // {
        //   text: 'About us',
        //   href: getPermalink('/about'),
        // },
        {
          text: 'Contact',
          href: getPermalink('/contact'),
        },
        {
          text: 'Terms',
          href: getPermalink('/terms'),
        },
        {
          text: 'Privacy policy',
          href: getPermalink('/privacy'),
        },
      ],
    },
    
    {
      text: 'ByteMath',
      links: [
        {
          text: 'Home',
          href: getBlogPermalink(),
        },
        {
          text: 'About Us',
          href: getPermalink("bytemath/about-us"),
        },
        { text: 'Tutorials', href: getPermalink('tutorials', 'category') },
        { text: 'Documentation', href: getPermalink('documentation', 'category') },
      ],
    },
  ],
  actions: [{ text: 'LogIn', href: '#', target: '_blank' }],
};

export const footerData = {
  links: [
    {
      title: 'Product',
      links: [
        {
          text: 'ByteMath',
          href: getBlogPermalink(),
        },
        {
          text: 'FiMa',
          href: getPermalink('/homes/fima'),
        },
        {
          text: 'Personal',
          href: getPermalink('/homes/personal'),
        },
      ],
    },
    {
      title: 'Other',
      links: [
        {
          text: 'Contact',
          href: getPermalink('/contact'),
        },
      ],
    },
    {
      title: 'Follow Us!',
      links: [
        { ariaLabel: 'Linkedin', icon: 'tabler:brand-linkedin', href: 'https://linkedin.com/in/zambelli-lorenzo' },
        { ariaLabel: 'Instagram', icon: 'tabler:brand-instagram', href: 'https://instagram.com/lorenzo_zambo' },
        { ariaLabel: 'Facebook', icon: 'tabler:brand-facebook', href: 'https://facebook.com/profile.php?id=100009454359495' },
        { ariaLabel: 'Gitlab', icon: 'tabler:brand-gitlab', href: 'https://gitlab.com/b64-Lorenzo' },
      ],
      isFlex: true,
    },
    {
      title: 'Legal',
      links: [
        { text: 'Terms', href: getPermalink('/terms') },
        { text: 'Privacy Policy', href: getPermalink('/privacy') },
      ],
    },
  ]
};
