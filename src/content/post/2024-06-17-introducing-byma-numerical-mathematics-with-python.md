---
templateKey: project-post
title: "Introducing ByMa: Numerical Mathematics with Python"
publishDate: 2024-06-17T06:18:03.694Z
excerpt: >-
  
  We are thrilled to announce the launch of **ByMa**, a groundbreaking Python package designed to enhance your numerical mathematics experience. ByMa integrates seamlessly with popular libraries such as NumPy and SciPy, offering an improved user interface and a suite of custom-built functions tailored to simplify complex mathematical computations. Whether you are a data scientist, engineer, or researcher, ByMa is here to streamline your workflow and boost your productivity. Dive into a new era of numerical analysis with ByMa and unlock the full potential of your mathematical endeavors.

image: ~/assets/images/ByteMath.svg
category: Documentation
tags:
  - ByMa
  - python
  - mathematics
---
[![PyPI - Version](https://img.shields.io/pypi/v/byma?style=plastic&label=ByMa&labelColor=green&color=blue&link=https%3A%2F%2Fbytemath.gitlab.io%2Fpython%2FByMa%2Findex.html)](https://pypi.org/project/byma/)

[![Static Badge](https://img.shields.io/badge/Docs-Read?style=plastic&label=Read&color=purple&link=https%3A%2F%2Fbytemath.gitlab.io%2Fpython%2FByMa%2Findex.html)](https://bytemath.gitlab.io/python/ByMa/index.html)



ByMa is a Python package designed to facilitate numerical mathematics tasks by implementing a range of standard methods, from iterative techniques to bifurcation methods. Noted for its simplicity, clarity, and efficiency, ByMa aims to enhance the learning experience for newcomers to numerical mathematics while streamlining the implementation and utilization of popular scientific libraries such as NumPy, SciPy, and Matplotlib.

## Installation

ByMa is best installed in a [virtual environment](https://docs.python.org/3/library/venv.html).

We state the most common steps for creating and using a virtual environment here.

Refer to the documentation for more details.

To create a virtual environment run

```

python3 -m venv /path/to/new/virtual/environment

```

and to activate the virtual environment, run

```

source /path/to/new/virtual/environment/bin/activate

```

After this, we can install ByMa from the pip package by using

```

pip install byma

```

In case the dependencies are not installed, you can run 

```

pip install -e .

```
