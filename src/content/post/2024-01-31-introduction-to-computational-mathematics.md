---
templateKey: project-post
title: Introduction to Computational Mathematics
publishDate: 2024-02-01T13:37:56.790Z
excerpt: >
  This book aims to provide an engaging introduction to computational
  mathematics through enjoyable simulations. Geared towards newcomers in the
  field of mathematics and applied mathematics, as well as those unfamiliar with
  the depth and beauty of mathematics, it serves as a gateway to understanding
  the subject.
image: ~/assets/images/designer.png
category: Tutorials
tags:
  - computational
  - introduction
  - general
---
[![Read the Book](https://img.shields.io/badge/Read-the%20Book-blue?style=for-the-badge&logo=bookmeter)](https://bytemath.lorenzozambelli.it/introduction-to-computational-mathematics) 

[![GitLab Repository](https://img.shields.io/badge/GitLab-Repository-orange?style=for-the-badge&logo=gitlab)](https://gitlab.com/ByteMath/mathematics/introduction-to-computational-mathematics)

## Goals

* Gain familiarity with the realm of (computational) mathematics
* Learn to execute your first Python code simulation
* Develop the ability to interpret and discuss simulation results


## Chapters

This book is divided into 3 main components:

* Introduction
* Mathematics background
* Getting started

But wait, why should you care about mathematics in the first place? Mathematics isn't just about numbers and equations; it's about **patterns**, **analogies**, and the skill to **change perspective**. 

In the Introduction, we delve into the significance of studying (applied) mathematics. We will conduct simulations of the Solar System, and we will explore various fields where computational mathematics plays a crucial role, such as Computational Fluid Dynamics and optimization.

The Mathematics Background section covers essential concepts necessary for simulating the Solar System and introduces fundamental concepts in computational mathematics, including convergence, stability, and iterative algorithms.

In the final chapter, I provide a brief overview of Python and Jupyter, along with instructions on how to locally run this book and its components. For those interested in further exploring Python, refer to the introductory series available [here](https://gitlab.com/ByteMath/python/introduction-to-python).
